package com.company.MainMenu;
import com.company.*;
import com.company.OOP.SecondMain;

import java.util.Scanner;

public class Menu {
    public static void main() {
        boolean running = true;
        Scanner input = new Scanner(System.in);

        System.out.println("👾");

        while (running) {
            System.out.println("Main menu - 0 for quit\n --> 1  Even or Odd number <--\n --> 2 Counting numbers 1-10 <--");
            System.out.println(" --> 3 Some random loop class <--\n --> 4 Forcing numbers <--\n --> 5 Factorial <--\n --> 6 Triangle Formula <--");
            System.out.println(" --> 7 Switch menu <--\n --> 8 Calculator <--\n --> 9 Finding the smallest number <--\n --> 10 Names in reverse <--\n --> 11 Find biggest and smallest <--");
            System.out.println(" --> 12 Methods <--");
            int choice = input.nextInt();


            if (choice == 1) {
                EvenOrOdd.main(1);
            }

            if (choice == 2) {
                Loops.counting();
            }

            if (choice == 3) {
                Loops.main();
            }
            if (choice == 4) {
                Loops.kapinasana();
            }
            if (choice == 5){
                Loops.factorial();
            }
            if (choice == 6) {
                Numbers.triangleformula();
            }
            if (choice == 7) {
                Switch.switchControl();
            }
            if (choice == 8){
                Numbers.calculator();
            }
            if (choice == 9){
                Switch.findSmallest();
            }
            if (choice == 10) {
                Switch.names();
            }
            if (choice == 11) {
                Switch.findBiggest();
            }
            if (choice == 12) {
                Methods.main();
            }
            if (choice == 0)
                running = false;
        }
    }
}
