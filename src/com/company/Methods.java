package com.company;

import java.util.Scanner;

public class Methods {

    public static void main() {
        Scanner input = new Scanner(System.in);


        System.out.println("Welcome to methods! \n --- Choose exercise ---");
        System.out.println("--- 1 Square \n ");
        int choice = input.nextInt();
        switch (choice) {
            case 1:
                System.out.println(square(5));
                break;
            case 2:
                verbally(2);
                break;
        }

    }


    public static int square(int x) {
        //int result = x*x;
        //System.out.println(result);
        return x * x;
    }

    public static void verbally(int number) {

        switch (number) {
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Three");
                break;
            case 4:
                System.out.println("Four");
                break;
            case 5:
                System.out.println("Five");
                break;
            case 6:
                System.out.println("Six");
                break;
            case 7:
                System.out.println("Seven");
                break;
            case 8:
                System.out.println("Eight");
                break;
            case 9:
                System.out.println("Nine");
                break;
            case 10:
                System.out.println("Ten");
                break;
        }
    }

    public static String reverse(String ToReverse) {
        String f = ToReverse.substring(0, 1);
        String s = ToReverse.substring(1, 2);
        String t = ToReverse.substring(2, 3);
        return t + s + f;
    }

}

