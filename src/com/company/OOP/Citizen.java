package com.company.OOP;

import com.company.InvalidDayException;
import com.company.InvalidMonthException;

public class Citizen {
    private String firstName;
    private String lastName;
    private Citizen spouse;
    private int year;
    private int month;
    private int day;

    public Citizen(String firstName, String lastName, int year, int month, int day)
            throws InvalidMonthException, InvalidDayException {
        if(month < 1 || month > 12) {
            throw new InvalidMonthException();
        }

        if(day < 1 || day > 30) {
            throw new InvalidDayException();
        }

        //4 6 9 11 = 30
        if (month == 4 || month == 6 || month == 9 || month == 11){
            if (day > 31){
                throw new InvalidDayException();
            }
        }

        //1 3 5 7 8 10 12 = 31
        if (month == 1 || month == 3 || month == 5|| month == 7|| month == 8|| month == 10|| month == 12){
            if(day < 1 || day > 31) {
                throw new InvalidDayException();
            }
        }



        this.firstName = firstName;
        this.lastName = lastName;
        this.year = year;
        this.month = month;
        this.day = day;
    }




    public Citizen(String firstName, String lastName, String personalNumber) {
        this.firstName = firstName;
        this.lastName = lastName;

        this.day = Integer.valueOf(personalNumber.substring(0, 2));
        this.month = Integer.valueOf(personalNumber.substring(2, 4));

        int century = Integer.valueOf(personalNumber.substring(7, 8));
        this.year = 1800 + century * 100 +
                Integer.valueOf(personalNumber.substring(4, 6));
    }




    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }





    public String toString() {
        return "Citizen: first name = " + this.firstName +
                ", last name = " + this.lastName +
                ", born at = " + this.day + "-" + this.month + "-" + this.year;
    }
}