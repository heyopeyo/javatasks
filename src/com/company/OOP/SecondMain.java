package com.company.OOP;

import com.company.InvalidDayException;
import com.company.InvalidMonthException;

public class SecondMain {

    public static void firstMain () {
        try {
            Citizen citizen1 = new Citizen("John", "Smith", 1955, 4, 31);
            Citizen citizen2 = new Citizen("Anna", "Potter", "080189-12602");

            System.out.println(citizen1);
            System.out.println(citizen2);

        } catch (InvalidDayException e) {
            System.out.println("Given day was invalid");

        } catch (InvalidMonthException e) {
            System.out.println("Given month was invalid");
        }

    }
}
