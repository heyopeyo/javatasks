package com.company;

import java.util.Scanner;

public class Loops {

    public static void main() {

        System.out.println("This is Loops Class");
        for (int i = 10;i >= 1; i--) {
            if (i % 2 == 0) {
                System.out.println(i);
            }

        }
    }

    public static void counting (){
        int result = 0;
        for (int i = 0; i <= 10; i++) {
           result = result + i;
            System.out.println(result);

        }
        System.out.println(result);
    }

    public  static void kapinasana () {
        Scanner forcing = new Scanner(System.in);
        System.out.println("Say a number");
        int a = forcing.nextInt();
        System.out.println("How many times to force?");
        int power = forcing.nextInt();
        int result = a;
        for (int i = 1; i < power; i++){
            result *= a;
        }
        System.out.println(result);
    }

    public static void factorial (){
        Scanner inputfac = new Scanner(System.in);

        System.out.println("Enter a number to factorize");
        int factorial = inputfac.nextInt();
        int result = 1;
        for (int i = 1; i <= factorial; i++){
            result *= i;
        }
        System.out.println(result);
    }
}
