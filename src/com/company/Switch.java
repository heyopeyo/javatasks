package com.company;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Switch {

    public static void switchControl () {
        Scanner input = new Scanner(System.in);
        System.out.println("Choose something!");
        int choice = input.nextInt();
        switch (choice) {
            case 1 :
                System.out.println("Works?");
                break;

            case 2 :
                System.out.println("This works too!");
                break;
        }
    }

    public static void findSmallest() {
        int[] numbers = {5,1,10};

        if (numbers.length != 0) {
            int smallest = numbers[0];

            int counter = 1;
            while (counter > numbers.length) {

                if (numbers[counter] > smallest) {
                    smallest = numbers[counter];
                }
                counter++;
            }
            System.out.println(smallest);
        } else {
            System.out.println("Array is empty");
        }
    }

    public static void names() {
        String[] names = new String[3];

        Scanner input = new Scanner(System.in);

        System.out.println("First name");
        names [0] = input.nextLine();
        System.out.println("Second name");
        names [1] = input.nextLine();
        System.out.println("Third name");
        names [2] = input.nextLine();
        System.out.println(names[2] + " " + names[1] + " " + names[0]);
    }

    public static void findBiggest() {
        int[] numbers = {5,1,10};

        if (numbers.length != 0) {
            int biggest = numbers[0];
            int smallest = numbers[0];


            int counter = 1;
            while (counter < numbers.length) {

                if (numbers[counter] > biggest) {
                    biggest = numbers[counter];
                }
                if (numbers[counter] < smallest) {
                    smallest = numbers[counter];
                }
                counter++;
            }
            System.out.println("Largest " + biggest + " Smallest " + smallest);
        } else {
            System.out.println("Array is empty");
        }
    }


}
